import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LibrosService } from 'src/app/services/libros.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styles: [
  ]
})
export class CrearComponent implements OnInit {

  crearLibroForm: FormGroup;
  serial: string = "";

  constructor(private fb: FormBuilder, 
    private librosService: LibrosService,
    private route: ActivatedRoute) { 

    this.crearLibroForm = this.fb.group({
      serial      : ['', Validators.required],
      titulo      : ['', Validators.required],
      categoria   : ['', Validators.required],
      num_paginas : ['', Validators.required],
      isbn        : ['', Validators.required],
      autor       : ['', Validators.required],
      editorial   : ['', Validators.required]
    })

  }

  ngOnInit(): void {

    this.serial = this.route.snapshot.paramMap.get('serial')!;

    if ( this.serial !== "new" ) {
      this.librosService.get(this.serial).subscribe(
        res => this.crearLibroForm.setValue(res)
      )
    }

  }

  invalidControl(controlName: string){
    const control = this.crearLibroForm.get(controlName);
    return control?.invalid && control.touched;
  }


  save(){

    Swal.fire({
      title: 'Guardando',
      icon: 'info',
      allowOutsideClick: false
    });
    Swal.showLoading();
  
    if (this.serial === "new"){
      this.librosService.create(this.crearLibroForm.value).subscribe();
    }else {
      this.librosService.update(this.crearLibroForm.value).subscribe();
    }

    Swal.fire({
      title: this.crearLibroForm.get('titulo')?.value,
      text: 'Registro correcto',
      icon: 'success'
    })
  }

}
