import { Component, OnInit } from '@angular/core';
import { LibroModel } from 'src/app/models/libro.model';
import { LibrosService } from 'src/app/services/libros.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styles: [
  ]
})
export class ListaComponent implements OnInit {

  libros: LibroModel[] = [];

  constructor( private librosService: LibrosService) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(){
    this.librosService.getAll().subscribe(
      res => this.libros = res
      );
  }

  delete( libro: LibroModel, i: number ){

    Swal.fire({
      title: "¿Estás seguro?",
      text: "Se va eliminar el libro " + libro.titulo,
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then( rta => {
        if (rta.isConfirmed) {
          this.libros.splice(i,1);
          this.librosService.delete(libro).subscribe();
        }
    });
    
  }

}
