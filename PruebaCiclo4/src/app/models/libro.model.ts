export class LibroModel {

  serial: number;
  titulo: string;
  categoria: string;
  num_paginas: number;
  isbn: string;
  autor: string;
  editorial: string;

  constructor( serial: number, titulo: string, categoria: string, num_paginas: number, isbn: string, autor: string, editorial: string ) {
    this.serial = serial;
    this.titulo = titulo;
    this.categoria = categoria;
    this.num_paginas = num_paginas;
    this.isbn = isbn;
    this.autor = autor;
    this.editorial = editorial;
  }

}