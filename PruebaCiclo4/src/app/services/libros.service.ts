import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { LibroModel } from '../models/libro.model';

@Injectable({
  providedIn: 'root'
})
export class LibrosService {

  url: string = "http://localhost:8000/";

  constructor(private http: HttpClient) { }

  create( libro: LibroModel ){
    return this.http.post(this.url+"libros/", libro);
  }

  update(libro: LibroModel ){
    return this.http.put(this.url + "libro/" + String(libro.serial), libro);
  }

  delete(libro: LibroModel ){
    return this.http.delete(this.url + "libro/" + String(libro.serial));
  }

  get(serial: string ){
    return this.http.get(this.url + "libro/" + serial);
  }

  getAll(){
    return this.http.get(this.url + "libros/").pipe( map( this.arrayObjToLibros )  );
  }

  private arrayObjToLibros(librosObj: any){
    const libros: LibroModel[] = [];
    for (let i = 0; i < librosObj.length; i++) {
      const libro: LibroModel = librosObj[i];
      libros.push(libro);
    }
    return libros;    
  }
}
