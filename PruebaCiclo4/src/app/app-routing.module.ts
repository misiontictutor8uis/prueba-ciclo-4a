import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearComponent } from './pages/libros/crear/crear.component';
import { ListaComponent } from './pages/libros/lista/lista.component';
import { NoPageFoundComponent } from './pages/no-page-found/no-page-found.component';

const routes: Routes = [
  { path: 'libros', component: ListaComponent },
  { path: 'libro/:serial', component: CrearComponent },
  { path: '', pathMatch: 'full', redirectTo: 'libros' },
  { path: '**', component: NoPageFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
