from django.db import models

class Libro(models.Model):
  serial = models.BigIntegerField(primary_key=True, serialize=True)
  titulo = models.CharField(max_length=64)
  categoria = models.CharField(max_length=64)
  num_paginas = models.IntegerField()
  isbn = models.CharField(max_length=64)
  autor = models.CharField(max_length=64)
  editorial = models.CharField(max_length=64)

  def __str__(self):
      return self.titulo